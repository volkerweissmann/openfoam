const word dictName("particleTrackDict");

#include "setConstantMeshDictionaryIO.hpp"

IOdictionary propsDict(dictIO);

word cloudName(propsDict.get<word>("cloud"));

List<word> userFields(propsDict.lookup("fields"));
