#include "fvMesh.hpp"

namespace Foam
{
    void writeFields
    (
        const fvMesh& mesh,
        const wordHashSet& selectedFields
    );
}
