// Some test code

#include "foamVersion.hpp"
#include "IOstreams.hpp"

namespace Foam
{
    void printTest()
    {
        Info<< nl;
        foamVersion::printBuildInfo(Info().stdStream());
    }
}
