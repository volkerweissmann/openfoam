if (finalIter)
{
    mesh.data::add("finalIteration", true);
}

if (frozenFlow)
{
    #include "EEqn.hpp"
}
else
{
    if (oCorr == 0)
    {
        #include "rhoEqn.hpp"
    }

    #include "UEqn.hpp"
    #include "YEqn.hpp"
    #include "EEqn.hpp"

    // --- PISO loop
    for (int corr=0; corr<nCorr; corr++)
    {
        #include "pEqn.hpp"
    }

    turbulence.correct();

    rho = thermo.rho();
}

if (finalIter)
{
    mesh.data::remove("finalIteration");
}
