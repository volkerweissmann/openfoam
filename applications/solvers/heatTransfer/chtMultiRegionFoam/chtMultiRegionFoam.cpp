/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2011-2016 OpenFOAM Foundation
    Copyright (C) 2017 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    chtMultiRegionFoam

Group
    grpHeatTransferSolvers

Description
    Transient solver for buoyant, turbulent fluid flow and solid heat
    conduction with conjugate heat transfer between solid and fluid regions.

    It handles secondary fluid or solid circuits which can be coupled
    thermally with the main fluid region. i.e radiators, etc.

\*---------------------------------------------------------------------------*/

#include "fvCFD.hpp"
#include "turbulentFluidThermoModel.hpp"
#include "rhoReactionThermo.hpp"
#include "CombustionModel.hpp"
#include "fixedGradientFvPatchFields.hpp"
#include "regionProperties.hpp"
#include "compressibleCourantNo.hpp"
#include "solidRegionDiffNo.hpp"
#include "solidThermo.hpp"
#include "radiationModel.hpp"
#include "fvOptions.hpp"
#include "coordinateSystem.hpp"
#include "loopControl.hpp"
#include "pressureControl.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    argList::addNote
    (
        "Transient solver for buoyant, turbulent fluid flow and solid heat"
        " conduction with conjugate heat transfer"
        " between solid and fluid regions."
    );

    #define NO_CONTROL
    #define CREATE_MESH createMeshesPostProcess.hpp
    #include "postProcess.hpp"

    #include "setRootCaseLists.hpp"
    #include "createTime.hpp"
    #include "createMeshes.hpp"
    #include "createFields.hpp"
    #include "initContinuityErrs.hpp"
    #include "createTimeControls.hpp"
    #include "readSolidTimeControls.hpp"
    #include "compressibleMultiRegionCourantNo.hpp"
    #include "solidRegionDiffusionNo.hpp"
    #include "setInitialMultiRegionDeltaT.hpp"

    while (runTime.run())
    {
        #include "readTimeControls.hpp"
        #include "readSolidTimeControls.hpp"
        #include "readPIMPLEControls.hpp"

        #include "compressibleMultiRegionCourantNo.hpp"
        #include "solidRegionDiffusionNo.hpp"
        #include "setMultiRegionDeltaT.hpp"

        ++runTime;

        Info<< "Time = " << runTime.timeName() << nl << endl;

        if (nOuterCorr != 1)
        {
            forAll(fluidRegions, i)
            {
                #include "storeOldFluidFields.hpp"
            }
        }

        // --- PIMPLE loop
        for (int oCorr=0; oCorr<nOuterCorr; ++oCorr)
        {
            const bool finalIter = (oCorr == nOuterCorr-1);

            forAll(fluidRegions, i)
            {
                Info<< "\nSolving for fluid region "
                    << fluidRegions[i].name() << endl;
                #include "setRegionFluidFields.hpp"
                #include "readFluidMultiRegionPIMPLEControls.hpp"
                #include "solveFluid.hpp"
            }

            forAll(solidRegions, i)
            {
                Info<< "\nSolving for solid region "
                    << solidRegions[i].name() << endl;
                #include "setRegionSolidFields.hpp"
                #include "readSolidMultiRegionPIMPLEControls.hpp"
                #include "solveSolid.hpp"
            }

            // Additional loops for energy solution only
            if (!oCorr && nOuterCorr > 1)
            {
                loopControl looping(runTime, pimple, "energyCoupling");

                while (looping.loop())
                {
                    Info<< nl << looping << nl;

                    forAll(fluidRegions, i)
                    {
                        Info<< "\nSolving for fluid region "
                            << fluidRegions[i].name() << endl;
                       #include "setRegionFluidFields.hpp"
                       #include "readFluidMultiRegionPIMPLEControls.hpp"
                       frozenFlow = true;
                       #include "solveFluid.hpp"
                    }

                    forAll(solidRegions, i)
                    {
                        Info<< "\nSolving for solid region "
                            << solidRegions[i].name() << endl;
                        #include "setRegionSolidFields.hpp"
                        #include "readSolidMultiRegionPIMPLEControls.hpp"
                        #include "solveSolid.hpp"
                    }
                }
            }
        }

        runTime.write();

        runTime.printExecutionTime(Info);
    }

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
