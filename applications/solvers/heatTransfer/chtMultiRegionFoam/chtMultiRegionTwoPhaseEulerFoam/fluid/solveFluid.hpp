if (finalIter)
{
    mesh.data::add("finalIteration", true);
}

if (frozenFlow)
{
    #include "EEqns.hpp"
}
else
{
    fluid.solve();
    fluid.correct();

    #include "YEqns.hpp"

    if (faceMomentum)
    {
        #include "pUf/UEqns.hpp"
        #include "EEqns.hpp"
        #include "pUf/pEqn.hpp"
    }
    else
    {
        #include "pU/UEqns.hpp"
        #include "EEqns.hpp"
        #include "pU/pEqn.hpp"
    }

    fluid.correctKinematics();

    // Update alpha's for new U
    fluid.correctTurbulence();
}

if (finalIter)
{
    mesh.data::remove("finalIteration");
}
