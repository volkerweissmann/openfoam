/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2011-2015 OpenFOAM Foundation
    Copyright (C) 2016-2017 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    overRhoPimpleDyMFoam

Group
    grpCompressibleSolvers grpMovingMeshSolvers

Description
    Transient solver for laminar or turbulent flow of compressible fluids
    for HVAC and similar applications.

    Uses the flexible PIMPLE (PISO-SIMPLE) solution for time-resolved and
    pseudo-transient simulations.

\*---------------------------------------------------------------------------*/

#include "fvCFD.hpp"
#include "dynamicFvMesh.hpp"
#include "fluidThermo.hpp"
#include "turbulentFluidThermoModel.hpp"
#include "bound.hpp"
#include "pimpleControl.hpp"
#include "pressureControl.hpp"
#include "CorrectPhi.hpp"
#include "fvOptions.hpp"
#include "localEulerDdtScheme.hpp"
#include "fvcSmooth.hpp"
#include "cellCellStencilObject.hpp"
#include "localMin.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    argList::addNote
    (
        "Transient solver for compressible turbulent flow.\n"
        "With optional mesh motion and mesh topology changes."
    );

    #include "setRootCaseLists.hpp"
    #include "createTime.hpp"
    #include "createDynamicFvMesh.hpp"
    #include "createDyMControls.hpp"
    #include "createRDeltaT.hpp"
    #include "initContinuityErrs.hpp"
    #include "createFields.hpp"
    #include "createMRF.hpp"
    #include "createFvOptions.hpp"
    #include "createRhoUfIfPresent.hpp"
    #include "createControls.hpp"

    turbulence->validate();

    if (!LTS)
    {
        #include "compressibleCourantNo.hpp"
        #include "setInitialDeltaT.hpp"
    }

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

    while (runTime.run())
    {
        #include "readControls.hpp"
        #include "readDyMControls.hpp"


        // Store divrhoU from the previous mesh so that it can be mapped
        // and used in correctPhi to ensure the corrected phi has the
        // same divergence
        autoPtr<volScalarField> divrhoU;
        if (correctPhi)
        {
            divrhoU.reset
            (
                new volScalarField
                (
                    "divrhoU",
                    fvc::div(fvc::absolute(phi, rho, U))
                )
            );
        }

        if (LTS)
        {
            #include "setRDeltaT.hpp"
        }
        else
        {
            #include "compressibleCourantNo.hpp"
            #include "setDeltaT.hpp"
        }

        ++runTime;

        Info<< "Time = " << runTime.timeName() << nl << endl;

        // --- Pressure-velocity PIMPLE corrector loop
        while (pimple.loop())
        {
            if (pimple.firstIter() || moveMeshOuterCorrectors)
            {

                // Do any mesh changes
                mesh.update();

                if (mesh.changing())
                {
                    MRF.update();

                    #include "setCellMask.hpp"

                    const surfaceScalarField faceMaskOld
                    (
                        localMin<scalar>(mesh).interpolate(cellMask.oldTime())
                    );

                    // Zero Uf on old faceMask (H-I)
                    rhoUf() *= faceMaskOld;

                    surfaceVectorField rhoUfint(fvc::interpolate(rho*U));

                    // Update Uf and phi on new C-I faces
                    rhoUf() += (1-faceMaskOld)*rhoUfint;

                    // Update Uf boundary
                    forAll(rhoUf().boundaryField(), patchI)
                    {
                        rhoUf().boundaryFieldRef()[patchI] =
                            rhoUfint.boundaryField()[patchI];
                    }

                    // Calculate absolute flux from the mapped surface velocity
                    phi = mesh.Sf() & rhoUf();

                    if (correctPhi)
                    {
                        #include "correctPhi.hpp"
                    }

                    // Zero phi on current H-I
                    const surfaceScalarField faceMask
                    (
                        localMin<scalar>(mesh).interpolate(cellMask)
                    );

                    phi *= faceMask;
                    U   *= cellMask;

                     // Make the fluxes relative to the mesh-motion
                    fvc::makeRelative(phi, rho, U);

                }

                if (checkMeshCourantNo)
                {
                    #include "meshCourantNo.hpp"
                }
            }

            if (pimple.firstIter() && !pimple.SIMPLErho())
            {
                #include "rhoEqn.hpp"
            }

            #include "UEqn.hpp"
            #include "EEqn.hpp"

            // --- Pressure corrector loop
            while (pimple.correct())
            {
                #include "pEqn.hpp"
            }

            if (pimple.turbCorr())
            {
                turbulence->correct();
            }
        }

        rho = thermo.rho();

        runTime.write();

        runTime.printExecutionTime(Info);
    }

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
