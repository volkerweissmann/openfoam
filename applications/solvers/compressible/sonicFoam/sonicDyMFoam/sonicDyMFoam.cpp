/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2011-2017 OpenFOAM Foundation
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    sonicDyMFoam

Group
    grpCompressibleSolvers grpMovingMeshSolvers

Description
    Transient solver for trans-sonic/supersonic, turbulent flow of a
    compressible gas, with optional mesh motion and mesh topology changes.

\*---------------------------------------------------------------------------*/

#include "fvCFD.hpp"
#include "dynamicFvMesh.hpp"
#include "psiThermo.hpp"
#include "turbulentFluidThermoModel.hpp"
#include "pimpleControl.hpp"
#include "CorrectPhi.hpp"
#include "fvOptions.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    argList::addNote
    (
        "Transient solver for trans-sonic/supersonic, turbulent flow"
        " of a compressible gas.\n"
        "With optional mesh motion and mesh topology changes."
    );

    #include "postProcess.hpp"

    #include "setRootCaseLists.hpp"
    #include "createTime.hpp"
    #include "createDynamicFvMesh.hpp"
    #include "createDyMControls.hpp"
    #include "createFields.hpp"
    #include "createFieldRefs.hpp"
    #include "createRhoUf.hpp"
    #include "compressibleCourantNo.hpp"
    #include "setInitialDeltaT.hpp"
    #include "initContinuityErrs.hpp"

    turbulence->validate();

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

    while (runTime.run())
    {
        #include "readDyMControls.hpp"

        {
            // Store divrhoU from the previous mesh so that it can be mapped
            // and used in correctPhi to ensure the corrected phi has the
            // same divergence
            volScalarField divrhoU
            (
                "divrhoU",
                fvc::div(fvc::absolute(phi, rho, U))
            );

            #include "compressibleCourantNo.hpp"
            #include "setDeltaT.hpp"

            ++runTime;

            Info<< "Time = " << runTime.timeName() << nl << endl;

            // Store momentum to set rhoUf for introduced faces.
            volVectorField rhoU("rhoU", rho*U);

            // Do any mesh changes
            mesh.update();

            if (mesh.changing())
            {
                MRF.update();

                if (correctPhi)
                {
                    // Calculate absolute flux from the mapped surface velocity
                    phi = mesh.Sf() & rhoUf;

                    #include "correctPhi.hpp"

                    // Make the fluxes relative to the mesh-motion
                    fvc::makeRelative(phi, rho, U);
                }
            }

            if (checkMeshCourantNo)
            {
                #include "meshCourantNo.hpp"
            }
        }

        #include "rhoEqn.hpp"
        Info<< "rho min/max : " << min(rho).value() << " " << max(rho).value()
            << endl;

        // --- Pressure-velocity PIMPLE corrector loop
        while (pimple.loop())
        {
            #include "UEqn.hpp"
            #include "EEqn.hpp"

            // --- Pressure corrector loop
            while (pimple.correct())
            {
                #include "pEqn.hpp"
            }

            if (pimple.turbCorr())
            {
                turbulence->correct();
            }
        }

        runTime.write();

        runTime.printExecutionTime(Info);
    }

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
