/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2011-2017 OpenFOAM Foundation
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    engineFoam

Description
    Transient solver for compressible, turbulent engine flow with a spray
    particle cloud.

\*---------------------------------------------------------------------------*/

#include "fvCFD.hpp"
#include "engineTime.hpp"
#include "engineMesh.hpp"
#include "turbulentFluidThermoModel.hpp"
#include "basicSprayCloud.hpp"
#include "psiReactionThermo.hpp"
#include "CombustionModel.hpp"
#include "radiationModel.hpp"
#include "SLGThermo.hpp"
#include "pimpleControl.hpp"
#include "fvOptions.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    argList::addNote
    (
        "Transient solver for compressible, turbulent engine flow"
        " with a spray particle cloud."
    );

    #define CREATE_TIME createEngineTime.hpp
    #define CREATE_MESH createEngineMesh.hpp
    #include "postProcess.hpp"

    #include "setRootCaseLists.hpp"
    #include "createEngineTime.hpp"
    #include "createEngineMesh.hpp"
    #include "createControl.hpp"
    #include "readEngineTimeControls.hpp"
    #include "createFields.hpp"
    #include "createFieldRefs.hpp"
    #include "createRhoUf.hpp"
    #include "compressibleCourantNo.hpp"
    #include "setInitialDeltaT.hpp"
    #include "initContinuityErrs.hpp"
    #include "startSummary.hpp"

    turbulence->validate();

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

    while (runTime.run())
    {
        #include "readEngineTimeControls.hpp"
        #include "compressibleCourantNo.hpp"
        #include "setDeltaT.hpp"

        ++runTime;

        Info<< "Engine time = " << runTime.theta() << runTime.unit() << endl;

        mesh.move();

        parcels.evolve();

        #include "rhoEqn.hpp"

        // --- Pressure-velocity PIMPLE corrector loop
        while (pimple.loop())
        {
            #include "UEqn.hpp"
            #include "YEqn.hpp"
            #include "EEqn.hpp"

            // --- Pressure corrector loop
            while (pimple.correct())
            {
                #include "pEqn.hpp"
            }

            if (pimple.turbCorr())
            {
                turbulence->correct();
            }
        }

        #include "logSummary.hpp"

        rho = thermo.rho();

        if (runTime.write())
        {
            combustion->Qdot()().write();
        }

        Info<< "ExecutionTime = " << runTime.elapsedCpuTime() << " s"
            << "  ClockTime = " << runTime.elapsedClockTime() << " s"
            << nl << endl;
    }

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
