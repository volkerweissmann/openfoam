/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2011-2017 OpenFOAM Foundation
    Copyright (C) 2018-2019 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    reactingParcelFoam

Group
    grpLagrangianSolvers

Description
    Transient solver for compressible, turbulent flow with a reacting,
    multiphase particle cloud, and surface film modelling.

\*---------------------------------------------------------------------------*/

#include "fvCFD.hpp"
#include "turbulentFluidThermoModel.hpp"

#include "surfaceFilmModel.hpp"
#include "rhoReactionThermo.hpp"
#include "CombustionModel.hpp"
#include "radiationModel.hpp"
#include "SLGThermo.hpp"
#include "fvOptions.hpp"
#include "pimpleControl.hpp"
#include "pressureControl.hpp"
#include "localEulerDdtScheme.hpp"
#include "fvcSmooth.hpp"
#include "cloudMacros.hpp"

#ifndef CLOUD_BASE_TYPE
    #define CLOUD_BASE_TYPE ReactingMultiphase
    #define CLOUD_BASE_TYPE_NAME "reacting"
#endif

#include CLOUD_INCLUDE_FILE(CLOUD_BASE_TYPE)
#define basicReactingTypeCloud CLOUD_TYPE(CLOUD_BASE_TYPE)

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    argList::addNote
    (
        "Transient solver for compressible, turbulent flow"
        " with reacting, multiphase particle clouds"
        " and surface film modelling."
    );

    #define CREATE_MESH createMeshesPostProcess.hpp
    #include "postProcess.hpp"

    #include "addCheckCaseOptions.hpp"
    #include "setRootCaseLists.hpp"
    #include "createTime.hpp"
    #include "createMesh.hpp"
    #include "createControl.hpp"
    #include "createTimeControls.hpp"
    #include "createFields.hpp"
    #include "createFieldRefs.hpp"
    #include "createRegionControls.hpp"
    #include "initContinuityErrs.hpp"

    turbulence->validate();

    if (!LTS)
    {
        #include "compressibleCourantNo.hpp"
        #include "setInitialDeltaT.hpp"
    }

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

    while (runTime.run())
    {
        #include "readTimeControls.hpp"

        if (LTS)
        {
            #include "setRDeltaT.hpp"
        }
        else
        {
            #include "compressibleCourantNo.hpp"
            #include "setMultiRegionDeltaT.hpp"
        }

        ++runTime;

        Info<< "Time = " << runTime.timeName() << nl << endl;

        parcels.evolve();
        surfaceFilm.evolve();

        if (solvePrimaryRegion)
        {
            if (pimple.nCorrPIMPLE() <= 1)
            {
                #include "rhoEqn.hpp"
            }

            // --- PIMPLE loop
            while (pimple.loop())
            {
                #include "UEqn.hpp"
                #include "YEqn.hpp"
                #include "EEqn.hpp"

                // --- Pressure corrector loop
                while (pimple.correct())
                {
                    #include "pEqn.hpp"
                }

                if (pimple.turbCorr())
                {
                    turbulence->correct();
                }
            }

            rho = thermo.rho();
        }

        runTime.write();

        runTime.printExecutionTime(Info);
    }

    Info<< "End" << endl;

    return 0;
}


// ************************************************************************* //
