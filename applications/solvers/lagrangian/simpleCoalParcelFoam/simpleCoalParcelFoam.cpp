/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2015-2016 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    simpleReactingParcelFoam

Group
    grpLagrangianSolvers

Description
    Steady-state solver for compressible, turbulent flow with coal particle
    clouds and optional sources/constraints.

\*---------------------------------------------------------------------------*/

#include "fvCFD.hpp"
#include "turbulentFluidThermoModel.hpp"
#include "coalCloud.hpp"
#include "rhoReactionThermo.hpp"
#include "CombustionModel.hpp"
#include "radiationModel.hpp"
#include "IOporosityModelList.hpp"
#include "fvOptions.hpp"
#include "SLGThermo.hpp"
#include "simpleControl.hpp"


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    argList::addNote
    (
        "Steady-state solver for compressible, turbulent flow"
        " with coal particle clouds and optional sources/constraints."
    );

    #include "postProcess.hpp"

    #include "addCheckCaseOptions.hpp"
    #include "setRootCaseLists.hpp"
    #include "createTime.hpp"
    #include "createMesh.hpp"
    #include "createControl.hpp"
    #include "createFields.hpp"
    #include "createFieldRefs.hpp"
    #include "createFvOptions.hpp"
    #include "initContinuityErrs.hpp"

    turbulence->validate();

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

    while (simple.loop())
    {
        Info<< "Time = " << runTime.timeName() << nl << endl;

        parcels.evolve();

        // --- Pressure-velocity SIMPLE corrector loop
        {
            #include "UEqn.hpp"
            #include "YEqn.hpp"
            #include "EEqn.hpp"
            #include "pEqn.hpp"
        }

        turbulence->correct();

        runTime.write();

        runTime.printExecutionTime(Info);
    }

    Info<< "End\n" << endl;

    return(0);
}


// ************************************************************************* //
