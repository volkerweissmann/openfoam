/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2011-2016 OpenFOAM Foundation
    Copyright (C) 2016-2018 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    overPimpleDyMFoam

Group
    grpIncompressibleSolvers grpMovingMeshSolvers

Description
    Transient solver for incompressible flow of Newtonian fluids
    on a moving mesh using the PIMPLE (merged PISO-SIMPLE) algorithm.

    Turbulence modelling is generic, i.e. laminar, RAS or LES may be selected.

\*---------------------------------------------------------------------------*/

#include "fvCFD.hpp"
#include "dynamicFvMesh.hpp"
#include "singlePhaseTransportModel.hpp"
#include "turbulentTransportModel.hpp"
#include "pimpleControl.hpp"
#include "fvOptions.hpp"

#include "cellCellStencilObject.hpp"
#include "zeroGradientFvPatchFields.hpp"
#include "localMin.hpp"
#include "interpolationCellPoint.hpp"
#include "transform.hpp"
#include "fvMeshSubset.hpp"
#include "oversetAdjustPhi.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    argList::addNote
    (
        "Transient solver for incompressible, turbulent flow"
        " on a moving mesh."
    );

    #include "postProcess.hpp"

    #include "setRootCaseLists.hpp"
    #include "createTime.hpp"
    #include "createDynamicFvMesh.hpp"
    #include "initContinuityErrs.hpp"

    pimpleControl pimple(mesh);

    #include "createFields.hpp"
    #include "createUf.hpp"
    #include "createMRF.hpp"
    #include "createFvOptions.hpp"
    #include "createControls.hpp"
    #include "CourantNo.hpp"
    #include "setInitialDeltaT.hpp"

    turbulence->validate();

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

    while (runTime.run())
    {
        #include "readControls.hpp"
        #include "CourantNo.hpp"

        #include "setDeltaT.hpp"

        ++runTime;

        Info<< "Time = " << runTime.timeName() << nl << endl;

        bool changed = mesh.update();

        if (changed)
        {
            #include "setCellMask.hpp"
            #include "setInterpolatedCells.hpp"

            surfaceScalarField faceMaskOld
            (
                localMin<scalar>(mesh).interpolate(cellMask.oldTime())
            );

            // Zero Uf on old faceMask (H-I)
            Uf *= faceMaskOld;
            // Update Uf and phi on new C-I faces
            Uf += (1-faceMaskOld)*fvc::interpolate(U);
            phi = mesh.Sf() & Uf;

            // Zero phi on current H-I
            surfaceScalarField faceMask
            (
                localMin<scalar>(mesh).interpolate(cellMask)
            );
            phi *= faceMask;
        }


        if (mesh.changing() && correctPhi)
        {
            // Calculate absolute flux from the mapped surface velocity
            #include "correctPhi.hpp"
        }

        // Make the flux relative to the mesh motion
        fvc::makeRelative(phi, U);

        if (mesh.changing() && checkMeshCourantNo)
        {
            #include "meshCourantNo.hpp"
        }

        // --- Pressure-velocity PIMPLE corrector loop
        while (pimple.loop())
        {
            #include "UEqn.hpp"

            // --- Pressure corrector loop
            while (pimple.correct())
            {
                #include "pEqn.hpp"
            }

            if (pimple.turbCorr())
            {
                laminarTransport.correct();
                turbulence->correct();
            }
        }

        runTime.write();

        runTime.printExecutionTime(Info);
    }

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
