/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2011-2014 OpenFOAM Foundation
    Copyright (C) 2016-2017 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    overInterDyMFoam

Group
    grpMultiphaseSolvers grpMovingMeshSolvers

Description
    Solver for two incompressible, isothermal immiscible fluids using a VOF
    (volume of fluid) phase-fraction based interface capturing approach,
    with optional mesh motion and mesh topology changes including adaptive
    re-meshing.

\*---------------------------------------------------------------------------*/

#include "fvCFD.hpp"
#include "dynamicFvMesh.hpp"
#include "CMULES.hpp"
#include "EulerDdtScheme.hpp"
#include "localEulerDdtScheme.hpp"
#include "CrankNicolsonDdtScheme.hpp"
#include "subCycle.hpp"
#include "immiscibleIncompressibleTwoPhaseMixture.hpp"
#include "turbulentTransportModel.hpp"
#include "pimpleControl.hpp"
#include "fvOptions.hpp"
#include "CorrectPhi.hpp"
#include "fvcSmooth.hpp"
#include "cellCellStencilObject.hpp"
#include "localMin.hpp"
#include "interpolationCellPoint.hpp"
#include "transform.hpp"
#include "fvMeshSubset.hpp"
#include "oversetAdjustPhi.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    argList::addNote
    (
        "Solver for two incompressible, isothermal immiscible fluids using"
        " VOF phase-fraction based interface capturing\n"
        "With optional mesh motion and mesh topology changes including"
        " adaptive re-meshing."
    );

    #include "postProcess.hpp"

    #include "setRootCaseLists.hpp"
    #include "createTime.hpp"
    #include "createDynamicFvMesh.hpp"
    #include "initContinuityErrs.hpp"
    pimpleControl pimple(mesh);
    #include "createTimeControls.hpp"
    #include "createDyMControls.hpp"
    #include "createFields.hpp"
    #include "createAlphaFluxes.hpp"
    #include "createFvOptions.hpp"

    volScalarField rAU
    (
        IOobject
        (
            "rAU",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh,
        dimensionedScalar("rAUf", dimTime/rho.dimensions(), 1.0)
    );

    #include "correctPhi.hpp"
    #include "createUf.hpp"

    turbulence->validate();

    if (!LTS)
    {
        #include "CourantNo.hpp"
        #include "setInitialDeltaT.hpp"
    }

    #include "setCellMask.hpp"
    #include "setInterpolatedCells.hpp"

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
    Info<< "\nStarting time loop\n" << endl;

    while (runTime.run())
    {
        #include "readControls.hpp"

        if (LTS)
        {
            #include "setRDeltaT.hpp"
        }
        else
        {
            #include "CourantNo.hpp"
            #include "alphaCourantNo.hpp"
            #include "setDeltaT.hpp"
        }

        ++runTime;

        Info<< "Time = " << runTime.timeName() << nl << endl;

        // --- Pressure-velocity PIMPLE corrector loop
        while (pimple.loop())
        {
            if (pimple.firstIter() || moveMeshOuterCorrectors)
            {
                scalar timeBeforeMeshUpdate = runTime.elapsedCpuTime();

                mesh.update();

                if (mesh.changing())
                {
                    Info<< "Execution time for mesh.update() = "
                        << runTime.elapsedCpuTime() - timeBeforeMeshUpdate
                        << " s" << endl;

                    // Do not apply previous time-step mesh compression flux
                    // if the mesh topology changed
                    if (mesh.topoChanging())
                    {
                        talphaPhi1Corr0.clear();
                    }

                    gh = (g & mesh.C()) - ghRef;
                    ghf = (g & mesh.Cf()) - ghRef;

                    // Update cellMask field for blocking out hole cells
                    #include "setCellMask.hpp"
                    #include "setInterpolatedCells.hpp"

                    const surfaceScalarField faceMaskOld
                    (
                        localMin<scalar>(mesh).interpolate(cellMask.oldTime())
                    );

                    // Zero Uf on old faceMask (H-I)
                    Uf *= faceMaskOld;

                    const surfaceVectorField Uint(fvc::interpolate(U));
                    // Update Uf and phi on new C-I faces
                    Uf += (1-faceMaskOld)*Uint;

                    // Update Uf boundary
                    forAll(Uf.boundaryField(), patchI)
                    {
                        Uf.boundaryFieldRef()[patchI] =
                            Uint.boundaryField()[patchI];
                    }

                    phi = mesh.Sf() & Uf;

                    // Correct phi on individual regions
                    if (correctPhi)
                    {
                         #include "correctPhi.hpp"
                    }

                    mixture.correct();

                    // Zero phi on current H-I
                    const surfaceScalarField faceMask
                    (
                        localMin<scalar>(mesh).interpolate(cellMask)
                    );
                    phi *= faceMask;
                    U   *= cellMask;

                    // Make the flux relative to the mesh motion
                    fvc::makeRelative(phi, U);

                }

                if (mesh.changing() && checkMeshCourantNo)
                {
                    #include "meshCourantNo.hpp"
                }
            }


            #include "alphaControls.hpp"
            #include "alphaEqnSubCycle.hpp"

            const surfaceScalarField faceMask
            (
                localMin<scalar>(mesh).interpolate(cellMask)
            );
            rhoPhi *= faceMask;

            mixture.correct();

            #include "UEqn.hpp"

            // --- Pressure corrector loop
            while (pimple.correct())
            {
                #include "pEqn.hpp"
            }

            if (pimple.turbCorr())
            {
                turbulence->correct();
            }
        }

        runTime.write();

        runTime.printExecutionTime(Info);
    }

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
