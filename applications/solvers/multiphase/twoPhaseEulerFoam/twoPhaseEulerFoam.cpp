/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2011-2017 OpenFOAM Foundation
    Copyright (C) 2020 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    twoPhaseEulerFoam

Group
    grpMultiphaseSolvers

Description
    Solver for a system of two compressible fluid phases with one dispersed
    phase. Eg, gas bubbles in a liquid including heat-transfer.

\*---------------------------------------------------------------------------*/

#include "fvCFD.hpp"
#include "twoPhaseSystem.hpp"
#include "PhaseCompressibleTurbulenceModel.hpp"
#include "pimpleControl.hpp"
#include "fvOptions.hpp"
#include "fixedValueFvsPatchFields.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    argList::addNote
    (
        "Solver for a system of two compressible fluid phases with one"
        " dispersed phase.\n"
        "Eg, gas bubbles in a liquid including heat-transfer."
    );

    #include "postProcess.hpp"

    #include "addCheckCaseOptions.hpp"
    #include "setRootCaseLists.hpp"
    #include "createTime.hpp"
    #include "createMesh.hpp"
    #include "createControl.hpp"
    #include "createFields.hpp"
    #include "createFieldRefs.hpp"
    #include "createTimeControls.hpp"
    #include "CourantNos.hpp"
    #include "setInitialDeltaT.hpp"

    bool faceMomentum
    (
        pimple.dict().getOrDefault("faceMomentum", false)
    );

    bool implicitPhasePressure
    (
        mesh.solverDict(alpha1.name()).getOrDefault
        (
            "implicitPhasePressure", false
        )
    );

    #include "pUf/createDDtU.hpp"
    #include "pU/createDDtU.hpp"

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

    while (runTime.run())
    {
        #include "readTimeControls.hpp"
        #include "CourantNos.hpp"
        #include "setDeltaT.hpp"

        ++runTime;
        Info<< "Time = " << runTime.timeName() << nl << endl;

        // --- Pressure-velocity PIMPLE corrector loop
        while (pimple.loop())
        {
            fluid.solve();
            fluid.correct();

            #include "contErrs.hpp"

            if (faceMomentum)
            {
                #include "pUf/UEqns.hpp"
                #include "EEqns.hpp"
                #include "pUf/pEqn.hpp"
                #include "pUf/DDtU.hpp"
            }
            else
            {
                #include "pU/UEqns.hpp"
                #include "EEqns.hpp"
                #include "pU/pEqn.hpp"
                #include "pU/DDtU.hpp"
            }

            if (pimple.turbCorr())
            {
                fluid.correctTurbulence();
            }
        }

        #include "write.hpp"

        runTime.printExecutionTime(Info);
    }

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
