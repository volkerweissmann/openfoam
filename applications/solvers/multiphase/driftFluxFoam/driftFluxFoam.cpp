/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2011-2017 OpenFOAM Foundation
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    driftFluxFoam

Group
    grpMultiphaseSolvers

Description
    Solver for two incompressible fluids using the mixture approach with
    the drift-flux approximation for relative motion of the phases.

    Used for simulating the settling of the dispersed phase and other
    similar separation problems.

\*---------------------------------------------------------------------------*/

#include "fvCFD.hpp"
#include "CMULES.hpp"
#include "subCycle.hpp"
#include "incompressibleTwoPhaseInteractingMixture.hpp"
#include "relativeVelocityModel.hpp"
#include "turbulenceModel.hpp"
#include "CompressibleTurbulenceModel.hpp"
#include "pimpleControl.hpp"
#include "fvOptions.hpp"
#include "gaussLaplacianScheme.hpp"
#include "uncorrectedSnGrad.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    argList::addNote
    (
        "Solver for two incompressible fluids using the mixture approach with"
        " the drift-flux approximation for relative motion of the phases.\n"
        "Used for simulating the settling of the dispersed phase and other"
        " similar separation problems."
    );

    #include "postProcess.hpp"

    #include "addCheckCaseOptions.hpp"
    #include "setRootCaseLists.hpp"
    #include "createTime.hpp"
    #include "createMesh.hpp"
    #include "createControl.hpp"
    #include "createTimeControls.hpp"
    #include "createFields.hpp"
    #include "initContinuityErrs.hpp"

    volScalarField& alpha2(mixture.alpha2());
    const dimensionedScalar& rho1 = mixture.rhod();
    const dimensionedScalar& rho2 = mixture.rhoc();
    relativeVelocityModel& UdmModel(UdmModelPtr());

    turbulence->validate();

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

    while (runTime.run())
    {
        #include "readTimeControls.hpp"
        #include "CourantNo.hpp"
        #include "setDeltaT.hpp"

        ++runTime;

        Info<< "Time = " << runTime.timeName() << nl << endl;

        // --- Pressure-velocity PIMPLE corrector loop
        while (pimple.loop())
        {
            #include "alphaControls.hpp"

            UdmModel.correct();

            #include "alphaEqnSubCycle.hpp"

            mixture.correct();

            #include "UEqn.hpp"

            // --- Pressure corrector loop
            while (pimple.correct())
            {
                #include "pEqn.hpp"
            }

            if (pimple.turbCorr())
            {
                turbulence->correct();
            }
        }

        runTime.write();

        runTime.printExecutionTime(Info);
    }

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
