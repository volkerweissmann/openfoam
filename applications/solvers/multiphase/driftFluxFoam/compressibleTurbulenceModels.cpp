/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2014-2016 OpenFOAM Foundation
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "CompressibleTurbulenceModel.hpp"
#include "incompressibleTwoPhaseInteractingMixture.hpp"
#include "addToRunTimeSelectionTable.hpp"
#include "makeTurbulenceModel.hpp"

#include "laminarModel.hpp"
#include "RASModel.hpp"
#include "LESModel.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

makeTurbulenceModelTypes
(
    geometricOneField,
    volScalarField,
    compressibleTurbulenceModel,
    CompressibleTurbulenceModel,
    incompressibleTwoPhaseInteractingMixture
);

makeBaseTurbulenceModel
(
    geometricOneField,
    volScalarField,
    compressibleTurbulenceModel,
    CompressibleTurbulenceModel,
    incompressibleTwoPhaseInteractingMixture
);

#define makeLaminarModel(Type)                                                 \
    makeTemplatedTurbulenceModel                                               \
    (                                                                          \
        incompressibleTwoPhaseInteractingMixtureCompressibleTurbulenceModel,   \
        laminar,                                                               \
        Type                                                                   \
    )

#define makeRASModel(Type)                                                     \
    makeTemplatedTurbulenceModel                                               \
    (                                                                          \
        incompressibleTwoPhaseInteractingMixtureCompressibleTurbulenceModel,   \
        RAS,                                                                   \
        Type                                                                   \
    )

#define makeLESModel(Type)                                                     \
    makeTemplatedTurbulenceModel                                               \
    (                                                                          \
        incompressibleTwoPhaseInteractingMixtureCompressibleTurbulenceModel,   \
        LES,                                                                   \
        Type                                                                   \
    )

#include "Stokes.hpp"
makeLaminarModel(Stokes);

#include "kEpsilon.hpp"
makeRASModel(kEpsilon);

#include "buoyantKEpsilon.hpp"
makeRASModel(buoyantKEpsilon);

#include "Smagorinsky.hpp"
makeLESModel(Smagorinsky);

#include "kEqn.hpp"
makeLESModel(kEqn);


// ************************************************************************* //
