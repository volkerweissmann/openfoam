/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2011-2016 OpenFOAM Foundation
    Copyright (C) 2020 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    multiphaseEulerFoam

Group
    grpMultiphaseSolvers

Description
    Solver for a system of many compressible fluid phases including
    heat-transfer.

\*---------------------------------------------------------------------------*/

#include "fvCFD.hpp"
#include "multiphaseSystem.hpp"
#include "phaseModel.hpp"
#include "dragModel.hpp"
#include "heatTransferModel.hpp"
#include "singlePhaseTransportModel.hpp"
#include "turbulentTransportModel.hpp"
#include "pimpleControl.hpp"
#include "IOMRFZoneList.hpp"
#include "CorrectPhi.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    argList::addNote
    (
        "Solver for a system of many compressible fluid phases including"
        " heat-transfer."
    );

    #include "postProcess.hpp"

    #include "addCheckCaseOptions.hpp"
    #include "setRootCaseLists.hpp"
    #include "createTime.hpp"
    #include "createMesh.hpp"
    #include "createControl.hpp"
    #include "createFields.hpp"
    #include "initContinuityErrs.hpp"
    #include "createTimeControls.hpp"
    #include "correctPhi.hpp"
    #include "CourantNo.hpp"
    #include "setInitialDeltaT.hpp"

    scalar slamDampCoeff
    (
        fluid.getOrDefault<scalar>("slamDampCoeff", 1)
    );

    dimensionedScalar maxSlamVelocity
    (
        "maxSlamVelocity",
        dimVelocity,
        GREAT,
        fluid
    );

    turbulence->validate();

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

    while (runTime.run())
    {
        #include "readTimeControls.hpp"
        #include "CourantNo.hpp"
        #include "setDeltaT.hpp"

        ++runTime;
        Info<< "Time = " << runTime.timeName() << nl << endl;

        // --- Pressure-velocity PIMPLE corrector loop
        while (pimple.loop())
        {
            turbulence->correct();
            fluid.solve();
            rho = fluid.rho();
            #include "zonePhaseVolumes.hpp"

            //#include "TEqns.hpp"
            #include "UEqns.hpp"

            // --- Pressure corrector loop
            while (pimple.correct())
            {
                #include "pEqn.hpp"
            }

            #include "DDtU.hpp"
        }

        runTime.write();

        runTime.printExecutionTime(Info);
    }

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
