/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2011-2017 OpenFOAM Foundation
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    coldEngineFoam

Group
    grpCombustionSolvers grpMovingMeshSolvers

Description
    Solver for cold-flow in internal combustion engines.

\*---------------------------------------------------------------------------*/

#include "fvCFD.hpp"
#include "engineTime.hpp"
#include "engineMesh.hpp"
#include "psiThermo.hpp"
#include "turbulentFluidThermoModel.hpp"
#include "OFstream.hpp"
#include "fvOptions.hpp"
#include "pimpleControl.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    argList::addNote
    (
        "Solver for cold-flow in internal combustion engines."
    );

    #define CREATE_TIME createEngineTime.hpp
    #define CREATE_MESH createEngineMesh.hpp
    #include "postProcess.hpp"

    #include "setRootCaseLists.hpp"
    #include "createEngineTime.hpp"
    #include "createEngineMesh.hpp"
    #include "createControl.hpp"
    #include "createFields.hpp"
    #include "createFieldRefs.hpp"
    #include "createRhoUf.hpp"
    #include "initContinuityErrs.hpp"
    #include "readEngineTimeControls.hpp"
    #include "compressibleCourantNo.hpp"
    #include "setInitialDeltaT.hpp"
    #include "startSummary.hpp"

    turbulence->validate();

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

    while (runTime.run())
    {
        #include "readEngineTimeControls.hpp"
        #include "compressibleCourantNo.hpp"
        #include "setDeltaT.hpp"

        ++runTime;

        Info<< "Engine time = " << runTime.theta() << runTime.unit()
            << endl;

        mesh.move();

        #include "rhoEqn.hpp"

        // --- Pressure-velocity PIMPLE corrector loop
        while (pimple.loop())
        {
            #include "UEqn.hpp"

            // --- Pressure corrector loop
            while (pimple.correct())
            {
                #include "EEqn.hpp"
                #include "pEqn.hpp"
            }

            if (pimple.turbCorr())
            {
                turbulence->correct();
            }
        }

        runTime.write();

        #include "logSummary.hpp"

        runTime.printExecutionTime(Info);
    }

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
