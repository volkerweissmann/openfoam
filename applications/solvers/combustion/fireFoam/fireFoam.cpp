/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2011-2017 OpenFOAM Foundation
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    fireFoam

Group
    grpCombustionSolvers

Description
    Transient solver for fires and turbulent diffusion flames with reacting
    particle clouds, surface film and pyrolysis modelling.

\*---------------------------------------------------------------------------*/

#include "fvCFD.hpp"
#include "turbulentFluidThermoModel.hpp"
#include "basicReactingCloud.hpp"
#include "surfaceFilmModel.hpp"
#include "pyrolysisModelCollection.hpp"
#include "radiationModel.hpp"
#include "SLGThermo.hpp"
#include "solidChemistryModel.hpp"
#include "psiReactionThermo.hpp"
#include "CombustionModel.hpp"
#include "pimpleControl.hpp"
#include "fvOptions.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    argList::addNote
    (
        "Transient solver for fires and turbulent diffusion flames"
        " with reacting particle clouds, surface film and pyrolysis modelling."
    );

    #include "postProcess.hpp"

    #include "addCheckCaseOptions.hpp"
    #include "setRootCaseLists.hpp"
    #include "createTime.hpp"
    #include "createMesh.hpp"
    #include "createControl.hpp"
    #include "createFields.hpp"
    #include "createFieldRefs.hpp"
    #include "initContinuityErrs.hpp"
    #include "createTimeControls.hpp"
    #include "compressibleCourantNo.hpp"
    #include "setInitialDeltaT.hpp"
    #include "createRegionControls.hpp"

    turbulence->validate();

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

    while (runTime.run())
    {
        #include "readTimeControls.hpp"
        #include "compressibleCourantNo.hpp"
        #include "solidRegionDiffusionNo.hpp"
        #include "setMultiRegionDeltaT.hpp"
        #include "setDeltaT.hpp"

        ++runTime;

        Info<< "Time = " << runTime.timeName() << nl << endl;

        parcels.evolve();

        surfaceFilm.evolve();

        if(solvePyrolysisRegion)
        {
            pyrolysis.evolve();
        }

        if (solvePrimaryRegion)
        {
            #include "rhoEqn.hpp"

            // --- PIMPLE loop
            while (pimple.loop())
            {
                #include "UEqn.hpp"
                #include "YEEqn.hpp"

                // --- Pressure corrector loop
                while (pimple.correct())
                {
                    #include "pEqn.hpp"
                }

                if (pimple.turbCorr())
                {
                    turbulence->correct();
                }
            }

            rho = thermo.rho();
        }

        runTime.write();

        runTime.printExecutionTime(Info);
    }

    Info<< "End" << endl;

    return 0;
}


// ************************************************************************* //
