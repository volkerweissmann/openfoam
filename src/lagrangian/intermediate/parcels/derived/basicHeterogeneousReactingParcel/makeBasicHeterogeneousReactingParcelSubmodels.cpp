/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2018 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "basicHeterogeneousReactingCloud.hpp"

#include "makeParcelCloudFunctionObjects.hpp"

// Kinematic
#include "makeThermoParcelForces.hpp" // thermo variant
#include "makeParcelDispersionModels.hpp"
#include "makeReactingParcelInjectionModels.hpp" // Reacting variant
#include "makeParcelPatchInteractionModels.hpp"
#include "makeParcelStochasticCollisionModels.hpp"
#include "makeReactingParcelSurfaceFilmModels.hpp" // Reacting variant
#include "makeHeterogeneousReactingParcelHeterogeneousReactingModels.hpp"

// Thermodynamic
#include "makeParcelHeatTransferModels.hpp"

// Reacting
#include "makeReactingMultiphaseParcelCompositionModels.hpp"
#include "makeReactingParcelPhaseChangeModels.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

makeParcelCloudFunctionObjects(basicHeterogeneousReactingCloud);

// Kinematic sub-models
makeThermoParcelForces(basicHeterogeneousReactingCloud);
makeParcelDispersionModels(basicHeterogeneousReactingCloud);
makeReactingParcelInjectionModels(basicHeterogeneousReactingCloud);
makeParcelPatchInteractionModels(basicHeterogeneousReactingCloud);
makeParcelStochasticCollisionModels(basicHeterogeneousReactingCloud);
makeReactingParcelSurfaceFilmModels(basicHeterogeneousReactingCloud);

// Thermo sub-models
makeParcelHeatTransferModels(basicHeterogeneousReactingCloud);

// Reacting sub-models
makeReactingMultiphaseParcelCompositionModels(basicHeterogeneousReactingCloud);
makeReactingParcelPhaseChangeModels(basicHeterogeneousReactingCloud);
makeHeterogeneousReactingParcelHeterogeneousReactingModels
(
    basicHeterogeneousReactingCloud
);

// ************************************************************************* //
