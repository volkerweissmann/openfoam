/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2013-2016 OpenFOAM Foundation
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "turbulentFluidThermoModels.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

makeBaseTurbulenceModel
(
    geometricOneField,
    volScalarField,
    compressibleTurbulenceModel,
    CompressibleTurbulenceModel,
    ThermalDiffusivity,
    fluidThermo
);


// -------------------------------------------------------------------------- //
// Laminar models
// -------------------------------------------------------------------------- //

#include "Stokes.hpp"
makeLaminarModel(Stokes);

#include "Maxwell.hpp"
makeLaminarModel(Maxwell);


// -------------------------------------------------------------------------- //
// RAS models
// -------------------------------------------------------------------------- //

#include "SpalartAllmaras.hpp"
makeRASModel(SpalartAllmaras);

#include "kEpsilon.hpp"
makeRASModel(kEpsilon);

#include "RNGkEpsilon.hpp"
makeRASModel(RNGkEpsilon);

#include "realizableKE.hpp"
makeRASModel(realizableKE);

#include "buoyantKEpsilon.hpp"
makeRASModel(buoyantKEpsilon);

#include "LaunderSharmaKE.hpp"
makeRASModel(LaunderSharmaKE);

#include "kOmega.hpp"
makeRASModel(kOmega);

#include "kOmegaSST.hpp"
makeRASModel(kOmegaSST);

#include "kOmegaSSTSAS.hpp"
makeRASModel(kOmegaSSTSAS);

#include "kOmegaSSTLM.hpp"
makeRASModel(kOmegaSSTLM);

#include "LRR.hpp"
makeRASModel(LRR);

#include "SSG.hpp"
makeRASModel(SSG);

#include "kEpsilonPhitF.hpp"
makeRASModel(kEpsilonPhitF);


// -------------------------------------------------------------------------- //
// LES models
// -------------------------------------------------------------------------- //

#include "Smagorinsky.hpp"
makeLESModel(Smagorinsky);

#include "WALE.hpp"
makeLESModel(WALE);

#include "kEqn.hpp"
makeLESModel(kEqn);

#include "dynamicKEqn.hpp"
makeLESModel(dynamicKEqn);

#include "dynamicLagrangian.hpp"
makeLESModel(dynamicLagrangian);

#include "SpalartAllmarasDES.hpp"
makeLESModel(SpalartAllmarasDES);

#include "SpalartAllmarasDDES.hpp"
makeLESModel(SpalartAllmarasDDES);

#include "SpalartAllmarasIDDES.hpp"
makeLESModel(SpalartAllmarasIDDES);

#include "DeardorffDiffStress.hpp"
makeLESModel(DeardorffDiffStress);

#include "kOmegaSSTDES.hpp"
makeLESModel(kOmegaSSTDES);

#include "kOmegaSSTDDES.hpp"
makeLESModel(kOmegaSSTDDES);

#include "kOmegaSSTIDDES.hpp"
makeLESModel(kOmegaSSTIDDES);


// ************************************************************************* //
