#ifndef faCFD_H
#define faCFD_H

#include "parRun.hpp"

#include "Time.hpp"
#include "faMesh.hpp"
#include "areaFields.hpp"
#include "edgeFields.hpp"
#include "faMatrices.hpp"
#include "fam.hpp"
#include "fac.hpp"
#include "volSurfaceMapping.hpp"

#include "OSspecific.hpp"
#include "argList.hpp"
#include "timeSelector.hpp"

#ifndef namespaceFoam
#define namespaceFoam
    using namespace Foam;
#endif

#endif
